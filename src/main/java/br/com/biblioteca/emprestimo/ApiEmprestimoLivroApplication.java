package br.com.biblioteca.emprestimo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ApiEmprestimoLivroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEmprestimoLivroApplication.class, args);
	}

}
