package br.com.biblioteca.emprestimo.core.usecase;

public interface BaseUseCase<I, O> {

	O executar (I input);
}
