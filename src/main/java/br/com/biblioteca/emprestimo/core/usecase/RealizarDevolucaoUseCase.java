package br.com.biblioteca.emprestimo.core.usecase;

import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoResponse;

public interface RealizarDevolucaoUseCase extends BaseUseCase<DevolucaoRequest, DevolucaoResponse> {

}
