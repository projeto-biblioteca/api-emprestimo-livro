package br.com.biblioteca.emprestimo.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.biblioteca.emprestimo.core.usecase.dto.ClienteResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.ListaErroEnum;
import br.com.biblioteca.emprestimo.core.usecase.dto.ResponseDataErro;
import br.com.biblioteca.emprestimo.core.usecase.gateway.BuscarClienteGateway;
import br.com.biblioteca.emprestimo.core.usecase.gateway.DevolverLivroGateway;

@Service
public class RealizarDevolucaoUseCaseImpl implements RealizarDevolucaoUseCase {

	private final BuscarClienteGateway buscarClienteGateway;
	private final DevolverLivroGateway devolverLivroGateway;

	public RealizarDevolucaoUseCaseImpl(BuscarClienteGateway buscarClienteGateway,
			DevolverLivroGateway devolverLivroGateway) {
		this.buscarClienteGateway = buscarClienteGateway;
		this.devolverLivroGateway = devolverLivroGateway;
	}

	@Override
	public DevolucaoResponse executar(DevolucaoRequest input) {
		
		DevolucaoResponse response = new DevolucaoResponse();
		
		Optional<ClienteResponse> responseBuscaCliente = buscarClienteGateway.buscarPorCpfCliente(input.getDisponivel());
		
		if(responseBuscaCliente.get().getNome() != null) {
			DevolucaoResponse responseRealizarDevolucao
													   = devolverLivroGateway.realizarDevolucao(input);
			
			if(responseRealizarDevolucao.getId() != null) {
				
				return responseRealizarDevolucao;
				
			} else {
				response.getResponse().adicionarErro(new ResponseDataErro(null, ListaErroEnum.LIVRO_NAO_ENCONTRADO)); 
			}
			
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro(null, ListaErroEnum.CLIENTE_NAO_ENCONTRADO)); 
		}
		
		return response;
	}

}
