package br.com.biblioteca.emprestimo.core.usecase;

import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoResponse;

public interface RealizarEmprestimoUseCase extends BaseUseCase<EmprestimoRequest, EmprestimoResponse> {

}
