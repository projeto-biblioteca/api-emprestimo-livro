package br.com.biblioteca.emprestimo.core.usecase;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.biblioteca.emprestimo.core.usecase.dto.ClienteResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.ListaErroEnum;
import br.com.biblioteca.emprestimo.core.usecase.dto.ResponseDataErro;
import br.com.biblioteca.emprestimo.core.usecase.gateway.BuscarClienteGateway;
import br.com.biblioteca.emprestimo.core.usecase.gateway.RealizarEmprestimoGateway;

@Service
public class RealizarEmprestimoUseCaseImpl implements RealizarEmprestimoUseCase {

	private final BuscarClienteGateway buscarClienteGateway;
	private final RealizarEmprestimoGateway realizarEmprestimoGateway;
	
	public RealizarEmprestimoUseCaseImpl(BuscarClienteGateway buscarClienteGateway,
			                             RealizarEmprestimoGateway realizarEmprestimoGateway) {
		this.buscarClienteGateway = buscarClienteGateway;
		this.realizarEmprestimoGateway = realizarEmprestimoGateway;
	}

	@Override
	public EmprestimoResponse executar(EmprestimoRequest input) {
		
		EmprestimoResponse response = new EmprestimoResponse();
		
		Optional<ClienteResponse> responseBuscaCliente = buscarClienteGateway.buscarPorCpfCliente(input.getDisponivel());
		
		if(responseBuscaCliente.get().getNome() != null) {
			EmprestimoResponse responseRealizarEmprestimo 
													   = realizarEmprestimoGateway.realizarEmprestimo(input);
			
			if(responseRealizarEmprestimo.getId() != null) {
				
				return responseRealizarEmprestimo;
				
			} else {
				response.getResponse().adicionarErro(new ResponseDataErro(null, ListaErroEnum.LIVRO_NAO_ENCONTRADO)); 
			}
			
		} else {
			response.getResponse().adicionarErro(new ResponseDataErro(null, ListaErroEnum.CLIENTE_NAO_ENCONTRADO)); 
		}
		
		return response;
	}

}
