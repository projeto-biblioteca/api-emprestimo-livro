package br.com.biblioteca.emprestimo.core.usecase.dto;

public class EmprestimoRequest {
	
	private String titulo;
	private Long disponivel;
	

	public Long getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Long cpf) {
		this.disponivel = cpf;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
