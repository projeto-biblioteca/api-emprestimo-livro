package br.com.biblioteca.emprestimo.core.usecase.dto;

import java.time.LocalDate;

public class EmprestimoResponse extends BaseResponse  {
	
	public EmprestimoResponse() {
		super();
	}
	
	public EmprestimoResponse(Long disponivel, String id, String titulo, String autor, String editora, LocalDate dataPublicacao, int numeroPaginas) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.editora = editora;
		this.dataPublicacao = dataPublicacao;
		this.numeroPaginas = numeroPaginas;
		this.id = id;
		this.disponivel = disponivel;
	}
	
	private String titulo;
	private String autor;
	private String editora;
	private LocalDate dataPublicacao;
	private int numeroPaginas;
	private String id;
	private Long disponivel;
	

	public Long getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Long disponivel) {
		this.disponivel = disponivel;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}
	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	public int getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

}
