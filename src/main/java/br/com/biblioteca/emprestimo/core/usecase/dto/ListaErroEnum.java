package br.com.biblioteca.emprestimo.core.usecase.dto;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS, DUPLICIDADE, OUTROS, LIVRO_NAO_ENCONTRADO, CLIENTE_NAO_ENCONTRADO, ENTIDADE_NAO_ENCONTRADA;
}
