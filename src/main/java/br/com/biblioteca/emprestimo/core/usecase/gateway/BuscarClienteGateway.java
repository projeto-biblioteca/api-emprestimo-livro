package br.com.biblioteca.emprestimo.core.usecase.gateway;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.biblioteca.emprestimo.core.usecase.dto.ClienteResponse;

@FeignClient(url="http://localhost:8080/api/v1/clientes/", name = "buscarCliente")
public interface BuscarClienteGateway {

	@GetMapping("{cpf}")
	Optional<ClienteResponse> buscarPorCpfCliente(@PathVariable("cpf") Long cpf);
	
}
