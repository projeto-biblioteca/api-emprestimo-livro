package br.com.biblioteca.emprestimo.core.usecase.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoResponse;
import feign.Headers;

@FeignClient(url="http://localhost:8081/api/v1/livros/devolucao", name = "realizarDevolucao")
public interface DevolverLivroGateway {

	@PostMapping
	@Headers("Content-Type: application/json")
	DevolucaoResponse realizarDevolucao(@RequestBody DevolucaoRequest emprestimo);
	
}
