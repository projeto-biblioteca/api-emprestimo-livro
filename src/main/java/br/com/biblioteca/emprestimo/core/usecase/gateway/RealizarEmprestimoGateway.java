package br.com.biblioteca.emprestimo.core.usecase.gateway;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoResponse;
import feign.Headers;

@FeignClient(url="http://localhost:8081/api/v1/livros/emprestimo", name = "relizarEmprestimo")
public interface RealizarEmprestimoGateway {

	@PostMapping
	@Headers("Content-Type: application/json")
	EmprestimoResponse realizarEmprestimo(@RequestBody EmprestimoRequest emprestimo);
	
}
