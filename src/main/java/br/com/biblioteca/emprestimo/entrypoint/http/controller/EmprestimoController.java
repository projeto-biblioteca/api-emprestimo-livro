package br.com.biblioteca.emprestimo.entrypoint.http.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.biblioteca.emprestimo.core.usecase.BaseUseCase;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.DevolucaoResponse;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoRequest;
import br.com.biblioteca.emprestimo.core.usecase.dto.EmprestimoResponse;

@RestController
@RequestMapping("/api/v1/gerenciamento")
public class EmprestimoController {
	
	private final BaseUseCase<EmprestimoRequest, EmprestimoResponse> usecaseEmprestimo;
	private final BaseUseCase<DevolucaoRequest, DevolucaoResponse> usecaseDevolucao;
	
	public EmprestimoController(BaseUseCase<EmprestimoRequest, EmprestimoResponse> usecaseEmprestimo, 
		                      	BaseUseCase<DevolucaoRequest, DevolucaoResponse> usecaseDevolucao) {
		this.usecaseEmprestimo = usecaseEmprestimo;
		this.usecaseDevolucao = usecaseDevolucao;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/emprestimo")
	public ResponseEntity<EmprestimoResponse> emprestarLivro(@RequestBody EmprestimoRequest request){
		
		EmprestimoResponse response = usecaseEmprestimo.executar(request);
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(value = "/devolucao")
	public ResponseEntity<DevolucaoResponse> devolverLivro(@RequestBody DevolucaoRequest request){
		
		DevolucaoResponse response = usecaseDevolucao.executar(request);
		return ResponseEntity.ok(response);
	}
}
